var $wh = $(window).height(); //screen hight
var $navbarHeight = $('.navbar').height(); //navbar height
var $blockdivHeight = $wh - $navbarHeight - 100;
$('#blocklyDiv').height($blockdivHeight); // apply height
$('#codeDiv').height($blockdivHeight); // apply height on codediv

var toolArea = document.getElementById('toolbox');
var blocklyDiv = document.getElementById('blocklyDiv');
var workspace = Blockly.inject(blocklyDiv, {
    toolbox: document.getElementById('toolbox'),
    comments: !0,
    disable: !0,
    collapse: !0,
});

// live Js

function myUpdateFunction(event) {
    var codeDiv = document.getElementById('codeDiv');
    var codeHolder = document.createElement('pre');
    codeHolder.className = 'prettyprint but-not-that-pretty';
    var code = document.createTextNode(Blockly.JavaScript.workspaceToCode(workspace));
    codeHolder.appendChild(code);
    codeDiv.replaceChild(codeHolder, codeDiv.lastElementChild);
    prettyPrint();
  }
workspace.addChangeListener(myUpdateFunction);
